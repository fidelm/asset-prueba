# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Widget_Cargando Asset.ui'
#
# Created: Tue Jun 24 18:42:31 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(293, 256)
        self.lbl_Percentage = QtGui.QLabel(Form)
        self.lbl_Percentage.setGeometry(QtCore.QRect(130, 60, 41, 20))
        self.lbl_Percentage.setObjectName("lbl_Percentage")
        self.progressBar = QtGui.QProgressBar(Form)
        self.progressBar.setGeometry(QtCore.QRect(40, 100, 211, 23))
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.label = QtGui.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(80, 160, 151, 16))
        self.label.setObjectName("label")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Publish Asset", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Percentage.setText(QtGui.QApplication.translate("Form", "## %", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Form", "Creating new Asset ...", None, QtGui.QApplication.UnicodeUTF8))

