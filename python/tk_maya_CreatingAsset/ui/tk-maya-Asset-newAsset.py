# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Widget_NewAsset.ui'
#
# Created: Tue Jun 24 11:04:37 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

#from PySide import QtCore, QtGui
#from PySide.QtGui import QMainWindow

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(295, 132)
        self.label = QtGui.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(20, 20, 121, 41))
        self.label.setObjectName("label")
        self.btn_Cancel = QtGui.QPushButton(Form)
        self.btn_Cancel.setGeometry(QtCore.QRect(50, 80, 91, 32))
        self.btn_Cancel.setObjectName("btn_Cancel")
        self.btn_Ok = QtGui.QPushButton(Form)
        self.btn_Ok.setGeometry(QtCore.QRect(170, 80, 91, 32))
        self.btn_Ok.setObjectName("btn_Ok")
        self.textEdit = QtGui.QTextEdit(Form)
        self.textEdit.setGeometry(QtCore.QRect(130, 30, 141, 21))
        self.textEdit.setObjectName("textEdit")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "New Asset", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Form", "Asset name:", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_Cancel.setText(QtGui.QApplication.translate("Form", "&Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_Ok.setText(QtGui.QApplication.translate("Form", "&Ok", None, QtGui.QApplication.UnicodeUTF8))

