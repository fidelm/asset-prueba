
import math

class Ui_Form_Principal_Asset(object):

    def setupUi(self, Form_Principal_Asset):
        Form_Principal_Asset.setObjectName("Form_Principal_Asset")
        Form_Principal_Asset.resize(311, 300)
        self.label = QtGui.QLabel(Form_Principal_Asset)
        self.label.setGeometry(QtCore.QRect(120, 10, 81, 16))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.radb_individual = QtGui.QRadioButton(Form_Principal_Asset)
        self.radb_individual.setGeometry(QtCore.QRect(60, 70, 91, 20))
        self.radb_individual.setObjectName("radb_individual")
        self.radb_grupal = QtGui.QRadioButton(Form_Principal_Asset)
        self.radb_grupal.setGeometry(QtCore.QRect(180, 70, 71, 20))
        self.radb_grupal.setObjectName("radb_grupal")
        self.label_2 = QtGui.QLabel(Form_Principal_Asset)
        self.label_2.setGeometry(QtCore.QRect(40, 40, 131, 16))
        self.label_2.setObjectName("label_2")
        self.btn_Next = QtGui.QPushButton(Form_Principal_Asset)
        self.btn_Next.setGeometry(QtCore.QRect(170, 240, 91, 32))
        self.btn_Next.setObjectName("btn_Next")
        self.btn_cancel = QtGui.QPushButton(Form_Principal_Asset)
        self.btn_cancel.setGeometry(QtCore.QRect(70, 240, 101, 32))
        self.btn_cancel.setObjectName("btn_cancel")
        self.lbl_Instruction = QtGui.QLabel(Form_Principal_Asset)
        self.lbl_Instruction.setGeometry(QtCore.QRect(60, 120, 191, 91))
        self.lbl_Instruction.setAutoFillBackground(False)
        self.lbl_Instruction.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_Instruction.setWordWrap(True)
        self.lbl_Instruction.setObjectName("lbl_Instruction")


        self.retranslateUi(Form_Principal_Asset)
        QtCore.QObject.connect(self.btn_cancel, QtCore.SIGNAL("clicked()"), Form_Principal_Asset.close)
        '''Aquiiiiii'''
        QtCore.QObject.connect(self.radb_individual, QtCore.SIGNAL("toggled(bool)"), self.lbl_Instruction.clear)
        QtCore.QObject.connect(self.radb_grupal, QtCore.SIGNAL("toggled(bool)"), self.lbl_Instruction.clear)
        '''Aquiiiiii'''
        QtCore.QObject.connect(self.radb_individual, QtCore.SIGNAL("toggled(bool)"), self.lbl_Instruction.setText("a"))
        QtCore.QObject.connect(self.radb_grupal, QtCore.SIGNAL("toggled(bool)"), self.lbl_Instruction.setText("b"))

        QtCore.QMetaObject.connectSlotsByName(Form_Principal_Asset)

    def retranslateUi(self, Form_Principal_Asset):
        Form_Principal_Asset.setWindowTitle(QtGui.QApplication.translate("Form_Principal_Asset", "Opciones de Asset", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Form_Principal_Asset", "New Asset", None, QtGui.QApplication.UnicodeUTF8))
        self.radb_individual.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Individual", None, QtGui.QApplication.UnicodeUTF8))
        self.radb_grupal.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Group", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Type of publish", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_Next.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Next ->", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_cancel.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Instruction.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Select a publish type", None, QtGui.QApplication.UnicodeUTF8))


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(295, 132)
        self.label = QtGui.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(20, 20, 121, 41))
        self.label.setObjectName("label")
        self.btn_Cancel = QtGui.QPushButton(Form)
        self.btn_Cancel.setGeometry(QtCore.QRect(50, 80, 91, 32))
        self.btn_Cancel.setObjectName("btn_Cancel")
        self.btn_Ok = QtGui.QPushButton(Form)
        self.btn_Ok.setGeometry(QtCore.QRect(170, 80, 91, 32))
        self.btn_Ok.setObjectName("btn_Ok")
        self.lineEdit = QtGui.QLineEdit(Form)
        self.lineEdit.setGeometry(QtCore.QRect(140, 30, 113, 21))
        self.lineEdit.setObjectName("lineEdit")

        self.retranslateUi(Form)
        QtCore.QObject.connect(self.btn_Cancel, QtCore.SIGNAL("clicked()"), Form.close)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "New Asset", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Form", "Asset name:", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_Cancel.setText(QtGui.QApplication.translate("Form", "&Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_Ok.setText(QtGui.QApplication.translate("Form", "&Ok", None, QtGui.QApplication.UnicodeUTF8))

def Master(AssetName):
    """
    Engloba las funciones principales
    """
    # Nombre del nuevo grupo
    NewGroup = AssetName + '_' + 'Geo'

    #Metodo que agrupa todos los elementos
    agrupar(NAME = AssetName)

    # El elemento seleccionado ya es un grupo
    moverAxisBaseCentro(cmds.ls(selection = True))

    CreacionCurva(cmds.ls(selection=True),AssetName)

    Form.close

def limpieza(objetin):
    """
        Metodo que hace la limpieza del objeto que se le manda
        Freeze tranformation en scale trasformation y rotation
    """
    # Hacer el freeze transformation
    cmds.makeIdentity(objetin, apply = True, translate = True, rotate = True, scale = True)

    # Borrar el historial
    cmds.delete (objetin, ch= True)

def renombrar (objetin, prefijo):
    """
        Metodo que renombra el objeto que se le manda dejando 
        su nombre original antecesido de un prefijo
    """
    cmds.rename(objetin, prefijo +  '_' +  str(objetin))
    return objetin

def agrupar(NAME):
    """
        Metodo que agrupa un arreglo de objetos y los mete en un 
        grupo con el nombre especificado y agrega los subfijos a cada
        elemento de ese nuevo grupo
    """
    for i in cmds.ls(selection = True):
        renombrar(i, NAME)
    for i in cmds.ls(selection = True):
        limpieza (i)
    cmds.group(cmds.ls(selection = True), name = NAME)

def moverAxisBaseCentro(objetin):
    """
    Se reciben tres parametros entre el 0 y el 1 para idnicar respecto al 
    asset donde estara el Axis
    """
    virtualBox = cmds.exactWorldBoundingBox(objetin)


    #  float[]  xmin, ymin, zmin, xmax, ymax, zmax.     exact worldBoundingBox
    #             0     1     2     3     4     5
    
    newX = ( virtualBox[0] + virtualBox[3] ) / 2
    newY = virtualBox[1] 
    newZ = ( virtualBox[2] + virtualBox[5] ) / 2
    nuevoAxis = [newX, newY, newZ]

    cmds.xform(objetin, piv=nuevoAxis, ws=True)

def CreacionCurva(objetin, NAME):
    """ 
        Metodo que crea una curva basandose en la posicion del Axis
        y el tamaño del bounding box del objeto seleccionado
    """
    NombreCurva = NAME + '_' + 'Master'
    center = cmds.xform (objetin, t = True, q = True)
    print center
    BoundingBox = cmds.exactWorldBoundingBox(objetin)
    #  float[]  xmin, ymin, zmin, xmax, ymax, zmax.     exact worldBoundingBox
    base = BoundingBox[3]-BoundingBox[0]
    height = BoundingBox[5]-BoundingBox[2]
    radio = (math.sqrt((base*base)+(height*height)))/2

    cmds.circle( nr=(0, 1, 0), c=(center[0], center[1], center[2]), r = radio)
    #cmds.delete(cmds.ls(selection=True), ch = True)

class NewAssetWindow(QMainWindow, Ui_Form):
    def __init__(self, parent= None):
        super(NewAssetWindow, self).__init__(parent)
        self.setupUi(self)
        self.btn_Ok.clicked.connect(self.Principal)


    def Principal(self):
        '''Principal'''
        #self.cajita.setText("Insercion de texto 1")
        nombreAsset= self.lineEdit.text()
        Master(nombreAsset)
        #self.close



class MainWindow(QMainWindow, Ui_Form_Principal_Asset):
    #nuevaVentana = NewAssetWindow()
    def __init__(self, parent= None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.btn_Next.clicked.connect(self.Principal)
        self.radb_individual.toggled(checked = true).connect(self.MensajeIndividual)
        self.radb_grupal.toggled(checked = true).connect(self.MensajeGrupal)

    def CargarVentanas(ventana1):
        ventana_1 = ventana1

    def Principal(self):
        '''Principal'''
        #nombreAsset= self.lineEdit.text()
        #Master(nombreAsset)
        nuevaVentana.show()
        frame.close()

    def MensajeIndividual(self):
        self.lbl_Instruction.setText("Mensajin Individual")
    

    def MensajeGrupal(self):
        self.lbl_Instruction.setText("Mensajin Grupal")

nuevaVentana = NewAssetWindow()
frame = MainWindow()

if __name__ == '__main__':    
    frame.show()

