# Cluster Studio
#   Junio 2014
#	Desarrollado por :
#		Fidel Moreno Miranda
#		fidelm@clusterstudio.com


import maya.cmds as cmds
import maya.mel as mel
import sys as sys

def ventana():
	"""
		Metodo creado con ayuda del codigo del script masterProxy3.py
		desarrollado por 
			Gabriel De Anda Licona 
			gabrielda@clusterstudio.com
	"""
	Nombre_Asset = cmds.promptDialog(message = 'Ingresa el nombre del Asset /n Chiquitin')

	if cmds.window('Ventanilla',q=1,ex=1):
		cmds.deleteUI('Ventanilla')

	window = cmds.window('Ventanilla', t = 'Create Master', wh = (200,200), rtf=1, s=0)

	cmds.columnLayout(adj=1)
	cmds.button(l='Ok',w=200,h=30,c='Master()')
	cmds.text('assetTxt',l='',align='center')
	cmds.showWindow(window)


def Master():
	"""
	No hace nada
	"""
	# Nombre del nuevo grupo
	nombreAsset = 'holi'
	NewGroup = nombreAsset + '_' + 'Geo'

	#Metodo que agrupa todos los elementos
	agrupar(NAME = NewGroup)

	# El elemento seleccionado ya es un grupo
	moverAxisBaseCentro(cmds.ls(selection = True))

	# Crear las curvas 

	cmds.deleteUI('Ventanilla')


def limpieza(objetin):
	"""
		Metodo que hace la limpieza del objeto que se le manda
		Freeze tranformation en scale trasformation y rotation
	"""
	# Hacer el freeze transformation
	cmds.makeIdentity(objetin, apply = True, translate = True, rotate = True, scale = True)

	# Borrar el historial
	cmds.delete (objetin, ch= True)

def renombrar (objetin, prefijo):
	"""
		Metodo que renombra el objeto que se le manda dejando 
		su nombre original antecesido de un prefijo
	"""
	cmds.rename(objetin, prefijo +  '_' +  str(objetin))
	return objetin

def agrupar(NAME):
	"""
		Metodo que agrupa un arreglo de objetos y los mete en un 
		grupo con el nombre especificado y agrega los subfijos a cada
		elemento de ese nuevo grupo
	"""
	for i in cmds.ls(selection = True):
		renombrar(i, NAME)
	for i in cmds.ls(selection = True):
		limpieza (i)
	cmds.group(cmds.ls(selection = True), name = NAME)

def moverAxisBaseCentro(objetin):
	"""
	Se reciben tres parametros entre el 0 y el 1 para idnicar respecto al 
	asset donde estara el Axis
	"""
	virtualBox = cmds.exactWorldBoundingBox(objetin)


	#  float[]	xmin, ymin, zmin, xmax, ymax, zmax.     exact worldBoundingBox
	#             0     1     2     3     4     5
	
	newX = ( virtualBox[0] + virtualBox[3] ) / 2
	newY = virtualBox[1] 
	newZ = ( virtualBox[2] + virtualBox[5] ) / 2
	nuevoAxis = [newX, newY, newZ]

	cmds.xform(objetin, piv=nuevoAxis, ws=True)

def CreacionCurva(objetin, NAME):
	""" 
		Metodo que crea una curva basandose en la posicion del Axis
		y el tamaño del bounding box del objeto seleccionado
	"""
	NombreCurva = NAME + '_' + 'LocalControl'
	center = cmds.xform (objetin, t = True, q = True)
	cmds.circle( nr=(0, 1, 0), c=(center[0], center[1], center[2]), r = 5)
	cmds.delete(cmds.ls(selection=True), ch = True)



# :::::::::::::::::::::::	
#         Main
# :::::::::::::::::::::::

def main():

	# Abre ventana de dialogo
	ventana()

# Programa
main()

