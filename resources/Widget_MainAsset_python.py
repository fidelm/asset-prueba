# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Widget_MainAsset.ui'
#
# Created: Wed Jul  2 10:57:02 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Form_Principal_Asset(object):
    def setupUi(self, Form_Principal_Asset):
        Form_Principal_Asset.setObjectName("Form_Principal_Asset")
        Form_Principal_Asset.resize(311, 300)
        self.label = QtGui.QLabel(Form_Principal_Asset)
        self.label.setGeometry(QtCore.QRect(120, 10, 81, 16))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.radb_individual = QtGui.QRadioButton(Form_Principal_Asset)
        self.radb_individual.setGeometry(QtCore.QRect(60, 70, 91, 20))
        self.radb_individual.setObjectName("radb_individual")
        self.radb_grupal = QtGui.QRadioButton(Form_Principal_Asset)
        self.radb_grupal.setGeometry(QtCore.QRect(180, 70, 71, 20))
        self.radb_grupal.setObjectName("radb_grupal")
        self.label_2 = QtGui.QLabel(Form_Principal_Asset)
        self.label_2.setGeometry(QtCore.QRect(40, 40, 131, 16))
        self.label_2.setObjectName("label_2")
        self.btn_Next = QtGui.QPushButton(Form_Principal_Asset)
        self.btn_Next.setGeometry(QtCore.QRect(170, 240, 91, 32))
        self.btn_Next.setObjectName("btn_Next")
        self.btn_cancel = QtGui.QPushButton(Form_Principal_Asset)
        self.btn_cancel.setGeometry(QtCore.QRect(70, 240, 101, 32))
        self.btn_cancel.setObjectName("btn_cancel")
        self.lbl_Instruction = QtGui.QLabel(Form_Principal_Asset)
        self.lbl_Instruction.setGeometry(QtCore.QRect(60, 120, 191, 91))
        self.lbl_Instruction.setAutoFillBackground(False)
        self.lbl_Instruction.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_Instruction.setWordWrap(True)
        self.lbl_Instruction.setObjectName("lbl_Instruction")

        self.retranslateUi(Form_Principal_Asset)
        QtCore.QObject.connect(self.btn_cancel, QtCore.SIGNAL("clicked()"), Form_Principal_Asset.close)
        QtCore.QObject.connect(self.radb_individual, QtCore.SIGNAL("toggled(bool)"), self.lbl_Instruction.clear)
        QtCore.QMetaObject.connectSlotsByName(Form_Principal_Asset)

    def retranslateUi(self, Form_Principal_Asset):
        Form_Principal_Asset.setWindowTitle(QtGui.QApplication.translate("Form_Principal_Asset", "Opciones de Asset", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Form_Principal_Asset", "New Asset", None, QtGui.QApplication.UnicodeUTF8))
        self.radb_individual.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Individual", None, QtGui.QApplication.UnicodeUTF8))
        self.radb_grupal.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Group", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Type of publish", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_Next.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Next ->", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_cancel.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.lbl_Instruction.setText(QtGui.QApplication.translate("Form_Principal_Asset", "Select a publish type", None, QtGui.QApplication.UnicodeUTF8))

